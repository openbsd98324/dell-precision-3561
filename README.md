# dell-precision-3561


## Specifications

![](medias/1671869589-screenshot.png)


![](medias/1671869853-screenshot.png)



## Docking 

Dell K20A001 

![](medias/20200302_195222__31447_K20A001.jpg)


## Ubuntu V1.0  (22.10)


![](medias/1671869703-1-22.10.jpg)


Grub location for EFI (2nd partition) /EFI/ubuntu/grub.cfg  

https://mirror.kumi.systems/ubuntureleases/22.10/ubuntu-22.10-desktop-amd64.iso

￼ 

## OpenSUSE Leap VM


![](medias/Screenshot_2022-12-16_23-41-51.png)






## Medias




![](medias/1671869283-1-left1-123.png)

![](medias/1671869283-2-right1-123.png)


## Alsamixer

![](medias/1673170244-screenshot.png)




